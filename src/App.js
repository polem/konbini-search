import React from 'react';
import { useState, useEffect } from 'react';
import SearchBar from './SearchBar';
import SearchResults from './SearchResults';
// import useAxios from 'axios-hooks'
import axios from 'axios'
import styled from '@emotion/styled';

const Container = styled.div`
  margin: 0 auto;
  margin: 2rem;
  text-align: center;
`

const Logo = styled.img`
  max-height: 5rem;
`

function App() {
  const [search, setSearch] = useState();
  const [results, setResults] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();

    async function fetchData () {
      setLoading(true);
      const response = await axios.get(
        `https://preprod-wp-www.konbini.com/fr/wp-json/wp/v2/posts/?search=${search}`, {
          cancelToken: source.token
        }
      ).catch((error) => {
        if (axios.isCancel(error)) {
          console.log('Request canceled', error.message);
        } else {
          console.error(error.message)
        }
      });

      setLoading(false);

      if (response && response.data) {
        setResults(response.data);
      }
    };

    fetchData();

    return () => {
      source.cancel(`Canceled search: ${search}`);
    };
  }, [search]);

  return (
    <Container>
      <header>
        <Logo src="https://static-cdn.konbini.com/konbini/img/headerLogo.png" alt="Konbini logo"/>
      </header>
      <main>
        <SearchBar onChange={setSearch} value={search} loading={loading}></SearchBar>
        <SearchResults results={results}></SearchResults>
      </main>
    </Container>
  );
}

export default App;
