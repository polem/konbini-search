import React from 'react';
import debounce from 'lodash/debounce';

import styled from '@emotion/styled';
import Loader from './Loader';

const StyledLoaderContainer = styled.div`
  position: absolute;
  right: 6rem;
  top: 2.2rem;
`;

const SearchBarContainer = styled.div`
  position: relative;
`

const Input = styled.input`
  box-sizing: border-box;
  border: none;
	outline: none;
	background: #fff;
	width: 90%;
	height: 1.6rem;
	margin: 0;
	z-index: 10;
	padding: 2rem 1.5rem;
	font-family: inherit;
	font-size: 20px;
  color: #2c3e50;
  box-shadow: 0 0 .8rem 0 rgb(101, 101, 101, .5);
  margin: 2rem 0;
  text-align: center;
`

function SearchBar(props) {
  const { value, onChange, loading = false } = props;

  const debouncedOnChange = debounce(onChange, 300, { trailing: true})

  const localOnChange = event => {
    const value = event.target.value;
    
    debouncedOnChange(value);
  }

  return (
    <SearchBarContainer>
      <Input type="text" name="search" placeholder="Rechercher un article" defaultValue={value} onChange={localOnChange}/>
      { loading && <StyledLoaderContainer><Loader/></StyledLoaderContainer> }
    </SearchBarContainer>
  );
}

export default SearchBar;
