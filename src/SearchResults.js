import React from 'react';
import { format } from 'date-fns';
import { fr as locale } from 'date-fns/locale'
import get from 'lodash/get'
import styled from '@emotion/styled';
import { css } from '@emotion/core';
import facepaint from 'facepaint';

const breakpoints = [576, 768, 992, 1200]

const mq = facepaint(
  breakpoints.map(bp => `@media (min-width: ${bp}px)`)
)

const expandedStyles = css(mq({
  flexBasis: ['100%', '50%', '33.3%', '25%']
}));

const SearchContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const SearchResult = styled.div`
  flex: 0 0;
  ${expandedStyles};
  box-sizing: border-box;
  padding: 1rem;
`

const SearchText = styled.div`
  text-align: center
`

const SearchThumbnailImg = styled.img`
  max-width: 60%;
`

const Link = styled.a`
  box-shadow: rgb(255, 189, 1) 0px -5px 0px inset;
  text-decoration: none;
    color: inherit;
`

function SearchResults (props) {
  const { results } = props;

  return (
    <SearchContainer>
      { results.map(item => (
        <SearchResult key={item.id}>
          <SearchThumbnailImg src={get(item, 'acf.post_thumbnail_vertical.sizes.medium')} />
          <SearchText>
            <h3><Link href={item.link}>{ item.title.rendered }</Link></h3>
            <span>{ format(new Date(item.date), 'PP', { locale }) }</span>
            <p>{ item.acf.chapeau }</p>
          </SearchText>
        </SearchResult>
      )) }
    </SearchContainer>
  );
}

export default SearchResults;